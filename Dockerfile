FROM microsoft/dotnet:2.2-sdk AS build
COPY . /build
WORKDIR /build
RUN dotnet build -o /app

FROM microsoft/dotnet:2.2-aspnetcore-runtime-alpine
WORKDIR /app
COPY --from=build /app /app
ENTRYPOINT ["dotnet", "sample-dotnet-http.dll"]